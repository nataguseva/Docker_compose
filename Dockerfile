FROM python:3.6-alpine AS build-env

WORKDIR /app

RUN apk add \
    gcc libc-dev linux-headers libpq-dev \
    && pip install --upgrade pip

COPY requirements.txt .

RUN pip wheel --no-cache-dir --no-deps --wheel-dir /wheels  -r requirements.txt

FROM python:3.6-alpine
COPY --from=build-env /wheels /wheels
COPY --from=build-env /usr/lib/libpq.so.5 /usr/lib/libpq.so.5

RUN pip install --upgrade pip \
    && pip install --no-cache /wheels/*

WORKDIR /app

EXPOSE 5000

ENTRYPOINT ["/bin/sh", "docker-entrypoint.sh"]




